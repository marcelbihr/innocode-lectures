package ch.innocode.example;

import ch.innocode.example.model.Gender;
import ch.innocode.example.model.PensionPlan;

public interface PensionPlanService {

	PensionPlan getPensionPlan(Gender gender);
	
}

package ch.innocode.example.impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import ch.innocode.example.EmployeeService;
import ch.innocode.example.model.Employee;
import ch.innocode.example.model.Gender;
import ch.innocode.example.model.Employee.EmployeeBuilder;

public class EmployeeServiceImpl implements EmployeeService {

	private static Map<String, Employee> employees= new HashMap<>();
	
	static {
		employees.put("Hans", EmployeeBuilder.create().name("Hans").birthdate(LocalDate.of(1990, 03, 01)).gender(Gender.MALE).build());
		employees.put("Rosmarie", EmployeeBuilder.create().name("Rosmarie").birthdate(LocalDate.of(1980, 04, 01)).gender(Gender.FEMALE).build());
		employees.put("Fritz", EmployeeBuilder.create().name("Fritz").birthdate(LocalDate.of(1970, 04, 01)).build());
	}
	
	@Override
	public Employee getEmployeeByName(String name) {
		return employees.get(name); 
	}
	
}



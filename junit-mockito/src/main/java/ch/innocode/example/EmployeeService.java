package ch.innocode.example;

import ch.innocode.example.model.Employee;

public interface EmployeeService {

	Employee getEmployeeByName(String name);

}
package ch.innocode.example.model;

public enum Gender {
	MALE, FEMALE, OTHER;
}
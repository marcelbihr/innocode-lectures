package ch.innocode.example.model;

import java.time.LocalDate;

public class Employee {

	private String name;
	private LocalDate birthdate;
	private Gender gender= Gender.OTHER;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public LocalDate getBirthdate() {
		return birthdate;
	}
	
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}
	
	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return "Employee [name=" + name + ", birthdate=" + birthdate
				+ ", gender=" + gender + "]";
	}


	public static class EmployeeBuilder {
		private Employee emp= new Employee();
		
		private EmployeeBuilder() {
		}
		
		public static EmployeeBuilder create() {
			return new EmployeeBuilder();
		}
		
		public EmployeeBuilder name(String name) {
			emp.setName(name);
			return this;
		}
		
		public EmployeeBuilder birthdate(LocalDate birthdate) {
			emp.setBirthdate(birthdate);
			return this;
		}
		
		/**
		 * @param gender
		 * @return
		 */
		public EmployeeBuilder gender(Gender gender) {
			emp.setGender(gender);
			return this;
		}
		
		public Employee build() {
			return emp;
		}
	}
	
}

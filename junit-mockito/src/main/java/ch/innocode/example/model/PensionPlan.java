package ch.innocode.example.model;

public class PensionPlan {

	private int ageOfPension;
	
	public PensionPlan(int ageOfPension) {
		this.ageOfPension= ageOfPension;
	}
	
	public int getAgeOfPension() {
		return ageOfPension;
	}
	
	public void setAgeOfPension(int ageOfPension) {
		this.ageOfPension= ageOfPension;
	}
	
}

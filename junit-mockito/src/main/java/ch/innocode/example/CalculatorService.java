package ch.innocode.example;

import java.time.LocalDate;

import javax.inject.Inject;

import ch.innocode.example.model.Employee;
import ch.innocode.example.model.PensionPlan;

public class CalculatorService {

	@Inject
	private EmployeeService employeeService;
	
	@Inject
	private PensionPlanService pensionPlanService;
	
	
	public int calculateYearsToWork(LocalDate calculationDate, String employeeName) {
		if (employeeName == null) {
			throw new IllegalArgumentException("Name must not be null");
		}
		if (calculationDate == null) {
			throw new IllegalArgumentException("Name must not be null");
		}
		
		Employee emp= employeeService.getEmployeeByName(employeeName);
		if (emp== null) {
			throw new IllegalArgumentException("No Employee found for this name");
		}
		
		PensionPlan plan= pensionPlanService.getPensionPlan(emp.getGender());
		if (plan== null) {
			throw new IllegalArgumentException("No Plan found for gender " + emp.getGender());
		}
		
		int ageOfEmployee= calculationDate.getYear() - emp.getBirthdate().getYear();
		if (ageOfEmployee < 0) { 
			throw new IllegalStateException("Employee " + emp.getName() + " is not born yet on calculation date " + calculationDate);
		}
		
		int yearsToWork= plan.getAgeOfPension() - ageOfEmployee;
		if (yearsToWork < 0) {
			return 0; // no work left
		} else {
			return yearsToWork;
		}
		
	}
	
	
}

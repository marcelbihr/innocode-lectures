package ch.innocode.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import ch.innocode.example.model.Employee;
import ch.innocode.example.model.Employee.EmployeeBuilder;
import ch.innocode.example.model.Gender;

public class EmployeeBuilderTest {

	@Test
	public void testEmployeeWithName() {
		Employee emp= EmployeeBuilder.create().name("Magdalena").build();
		assertEquals("Magdalena", emp.getName());
		assertNull(emp.getBirthdate());
		assertEquals(Gender.OTHER, emp.getGender());
		
	}
	
}

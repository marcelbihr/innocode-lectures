package ch.innocode.example;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import ch.innocode.example.impl.EmployeeServiceImpl;
import ch.innocode.example.model.Employee.EmployeeBuilder;
import ch.innocode.example.model.Gender;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

	@Spy
	private EmployeeService employeeService= new EmployeeServiceImpl();
	
	@Test
	public void testExistingValues() {
		
		assertEquals(LocalDate.of(1990, 03, 01), employeeService.getEmployeeByName("Hans").getBirthdate());
		assertEquals(Gender.FEMALE, employeeService.getEmployeeByName("Rosmarie").getGender());
		
	}
	
	@Test
	public void testSpyvalues() {
		
		
		when(employeeService.getEmployeeByName("Fridolin")).thenReturn(EmployeeBuilder.create().name("Freddy Hinz").gender(Gender.MALE).build());
		
		assertEquals(LocalDate.of(1990, 03, 01), employeeService.getEmployeeByName("Hans").getBirthdate());

		assertEquals("Freddy Hinz", employeeService.getEmployeeByName("Fridolin").getName());
		
	}
	
	
}

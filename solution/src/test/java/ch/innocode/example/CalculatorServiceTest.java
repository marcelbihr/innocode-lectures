package ch.innocode.example;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ch.innocode.example.model.Employee;
import ch.innocode.example.model.Employee.EmployeeBuilder;
import ch.innocode.example.model.Gender;
import ch.innocode.example.model.PensionPlan;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorServiceTest {

	@Mock
	private EmployeeService employeeService;
	
	@Mock
	private PensionPlanService pensionPlanService;
	
	@InjectMocks
	private CalculatorService calculatorService;
	
	@Test
	public void test20YearsLeft() {
		
		LocalDate calulationDate= LocalDate.of(2018, 01, 01);
		LocalDate fortyFive= LocalDate.of(2018-45, 01, 01);
		Employee emp= EmployeeBuilder.create().birthdate(fortyFive).gender(Gender.MALE).build();
		
		when(employeeService.getEmployeeByName(anyString())).thenReturn(emp);
		
		when(pensionPlanService.getPensionPlan(Gender.MALE)).thenReturn(new PensionPlan(65));
		
		
		int yearsLeft = calculatorService.calculateYearsToWork(calulationDate, "Hurlibutz");
		
		assertEquals(20, yearsLeft);
	}
	
	@Test
	public void testCaptor() {
		
		LocalDate calulationDate= LocalDate.of(2018, 01, 01);
		LocalDate fortyFive= LocalDate.of(2018-45, 01, 01);
		Employee emp= EmployeeBuilder.create().birthdate(fortyFive).gender(Gender.MALE).build();
		
		ArgumentCaptor<String> nameCaptor= ArgumentCaptor.forClass(String.class);
		
		when(employeeService.getEmployeeByName(nameCaptor.capture())).thenReturn(emp);
		
		when(pensionPlanService.getPensionPlan(Gender.MALE)).thenReturn(new PensionPlan(65));
		
		
		int yearsLeft = calculatorService.calculateYearsToWork(calulationDate, "Hurlibutz");
		
		assertEquals("Hurlibutz", nameCaptor.getValue());
		
		assertEquals(20, yearsLeft);
		
	}
	
	
	@Test
	public void testVerify() {
		
		LocalDate calulationDate= LocalDate.of(2018, 01, 01);
		LocalDate fortyFive= LocalDate.of(2018-45, 01, 01);
		Employee emp= EmployeeBuilder.create().birthdate(fortyFive).gender(Gender.MALE).build();
		
		when(employeeService.getEmployeeByName(anyString())).thenReturn(emp);
		
		when(pensionPlanService.getPensionPlan(Gender.MALE)).thenReturn(new PensionPlan(65));
		
		int yearsLeft = calculatorService.calculateYearsToWork(calulationDate, "Hurlibutz");
		
		assertEquals(20, yearsLeft);
		verify(pensionPlanService, times(1)).getPensionPlan(any(Gender.class));
		
	}
	
	
	
	

}
